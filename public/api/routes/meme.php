<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

$app->post('/meme', function (Request $request, Response $response, array $args) {

  $fields = $request->getParsedBody();

  switch ($fields['format']) {
    case "facebook":
      $targetImage = imagecreatetruecolor(1200, 630);
      break;
    case "twitter":
      $targetImage = imagecreatetruecolor(1024, 512);
      break;
    case "instagram":
      $targetImage = imagecreatetruecolor(1080, 1080);
      break;
    default:
      return $response->withJson(array(
        'success' => false,
        'message' => 'Ungültiges Format.'
      ), 400);
  }
  if (!is_numeric($fields['layout']) || $fields['layout'] < 1 || $fields['layout'] > 4) {
    return $response->withJson(array(
      'success' => false,
      'message' => 'Ungültiges Layout.'
    ), 400);
  }

  imagesavealpha($targetImage, true);
  $transparent = imagecolorallocatealpha($targetImage, 0, 0, 0, 127);
  imagefill($targetImage, 0, 0, $transparent);

  $tmpDir = rtrim($this->settings['filePath'], '/') . '/_meme_tmp/';
  if (!file_exists($tmpDir)) {
    mkdir($tmpDir);
  }

  if ($fields['image'] == 'upload') {
    if (empty($fields['file']) || empty($fields['filename'])) {
      imagedestroy($targetImage);
      return $response->withJson(array(
        'success' => false,
        'message' => 'Bitte wähle ein Bild aus.'
      ), 500);
    }

    $filenameSuffix = preg_replace("/^.*\.(.+?)$/", "$1", strtolower($fields['filename']));
    $uploadedFile = $tmpDir . 'upload_' . uniqid() . '.' . $filenameSuffix;
    $imgData = str_replace(' ', '+', $fields['file']);
    $imgData = substr($imgData, strpos($imgData, ",") + 1);
    $imgData = base64_decode($imgData);
    file_put_contents($uploadedFile, $imgData);
    switch ($filenameSuffix) {
      case "jpg":
      case "jpeg":
        $uploadedImage = imagecreatefromjpeg($uploadedFile);
        break;
      case "png":
        $uploadedImage = imagecreatefrompng($uploadedFile);
        break;
      default:
        imagedestroy($targetImage);
        unlink($uploadedFile);
        return $response->withJson(array(
          'success' => false,
          'message' => 'Fehler beim Datei-Upload. Versuche es später noch einmal oder probiere es mit einer anderen Datei.'
        ), 500);
    }

    // rotate
    $exif = exif_read_data($uploadedFile);
    $rotatedImage = $uploadedImage;
    if (!empty($exif['Orientation'])) {
      switch ($exif['Orientation']) {
        case 3:
          $rotatedImage = imagerotate($uploadedImage, 180, 0);
          imagedestroy($uploadedImage);
          break;
        case 6:
          $rotatedImage = imagerotate($uploadedImage, -90, 0);
          imagedestroy($uploadedImage);
          break;
        case 8:
          $rotatedImage = imagerotate($uploadedImage, 90, 0);
          imagedestroy($uploadedImage);
          break;
      }
    }
    unlink($uploadedFile);

    $uploadedWidth = imagesx($rotatedImage);
    $uploadedHeight = imagesy($rotatedImage);
    if ($uploadedWidth / $uploadedHeight > imagesx($targetImage) / imagesy($targetImage)) {
      $targetHeight = imagesy($targetImage);
      $targetWidth = abs($targetHeight * ($uploadedWidth / $uploadedHeight));

      $dst_x = 0;
      $src_x = 0;
      if (imagesx($targetImage) < $targetWidth) {
        $src_x = ($targetWidth - imagesx($targetImage)) / 2 / ($targetHeight / $uploadedHeight);
      } else {
        $dst_x = (imagesx($targetImage) - $targetWidth) / 2 / ($targetHeight / $uploadedHeight);
      }
      imagecopyresampled($targetImage, $rotatedImage, $dst_x, 0, $src_x, 0, $targetWidth, $targetHeight, $uploadedWidth, $uploadedHeight);
    } else {
      $targetWidth = imagesx($targetImage);
      $targetHeight = abs($targetWidth * ($uploadedHeight / $uploadedWidth));

      $dst_y = 0;
      $src_y = 0;
      if (imagesy($targetImage) < $targetHeight) {
        $src_y = ($targetHeight - imagesy($targetImage)) / 2 / ($targetHeight / $uploadedHeight);
      } else {
        $dst_y = (imagesy($targetImage) - $targetHeight) / 2 / ($targetHeight / $uploadedHeight);
      }
      imagecopyresampled($targetImage, $rotatedImage, 0, $dst_y, 0, $src_y, $targetWidth, $targetHeight, $uploadedWidth, $uploadedHeight);
    }
    imagedestroy($rotatedImage);

  } else {
    $cleanedFilename = preg_replace("/\W/", "", strtolower($fields['image']));
    $imagePath = realpath(__DIR__ . '/../resources/meme/img') . '/' . $cleanedFilename . '-' . $fields['format'] . '.png';
    if (file_exists($imagePath)) {
      $image = imagecreatefrompng($imagePath);
      imagecopyresampled($targetImage, $image, 0, 0, 0, 0, imagesx($targetImage), imagesy($targetImage), imagesx($image), imagesy($image));
      imagedestroy($image);
    } else {
      $cleanedFilename = preg_replace("/[^a-zA-Z0-9_-]/", "", strtolower($fields['image']));
      $imagePath = realpath(__DIR__ . '/../resources/meme/img') . '/' . $cleanedFilename . '.jpg';
      if (file_exists($imagePath)) {
        $loadedImage = imagecreatefromjpeg($imagePath);
        $loadedWidth = imagesx($loadedImage);
        $loadedHeight = imagesy($loadedImage);

        if ($loadedWidth / $loadedHeight > imagesx($targetImage) / imagesy($targetImage)) {
          $targetHeight = imagesy($targetImage);
          $targetWidth = abs($targetHeight * ($loadedWidth / $loadedHeight));

          $dst_x = 0;
          $src_x = 0;
          if (imagesx($targetImage) < $targetWidth) {
            $src_x = ($targetWidth - imagesx($targetImage)) / 2 / ($targetHeight / $loadedHeight);
          } else {
            $dst_x = (imagesx($targetImage) - $targetWidth) / 2 / ($targetHeight / $loadedHeight);
          }
          imagecopyresampled($targetImage, $loadedImage, $dst_x, 0, $src_x, 0, $targetWidth, $targetHeight, $loadedWidth, $loadedHeight);
        } else {
          $targetWidth = imagesx($targetImage);
          $targetHeight = abs($targetWidth * ($loadedHeight / $loadedWidth));

          $dst_y = 0;
          $src_y = 0;
          if (imagesy($targetImage) < $targetHeight) {
            $src_y = ($targetHeight - imagesy($targetImage)) / 2 / ($targetHeight / $loadedHeight);
          } else {
            $dst_y = (imagesy($targetImage) - $targetHeight) / 2 / ($targetHeight / $loadedHeight);
          }
          imagecopyresampled($targetImage, $loadedImage, 0, $dst_y, 0, $src_y, $targetWidth, $targetHeight, $loadedWidth, $loadedHeight);
        }
        imagedestroy($loadedImage);
      } else {
        imagedestroy($targetImage);
        return $response->withJson(array(
          'success' => false,
          'message' => 'Ungültiges Bild.'
        ), 400);
      }
    }

  }

  // Header & Footer
  if ($fields['image'] != 'empty') {
    $image = imagecreatefrompng(realpath(__DIR__ . '/../resources/meme/img') . '/header-footer-' . $fields['format'] . '.png');
    imagecopyresampled($targetImage, $image, 0, 0, 0, 0, imagesx($targetImage), imagesy($targetImage), imagesx($image), imagesy($image));
    imagedestroy($image);
  }

  $textLayer = imagecreatetruecolor(imagesx($targetImage), imagesy($targetImage));
  imagefill($textLayer, 0, 0, $transparent);

  $fontPoliticsHead = realpath(__DIR__ . '/../resources/meme/fonts') . '/PoliticsHead-Bold.ttf';
  $fontRoboto = realpath(__DIR__ . '/../resources/meme/fonts') . '/Roboto-Bold.ttf';
  $black = imagecolorallocate($textLayer, 0, 0, 0);
  $purple = imagecolorallocate($textLayer, 101, 36, 128);
  $boxColor = imagecolorallocatealpha($textLayer, 255, 255, 255,
    25.4 /* 127x0.2 */);

  // hashtags
  if (!empty($fields['hashtags'])) {
    $text_encoding = mb_detect_encoding($fields['hashtags'], 'UTF-8, ISO-8859-1');
    if ($text_encoding != 'UTF-8') {
      $fields['hashtags'] = mb_convert_encoding($fields['hashtags'], 'UTF-8', $text_encoding);
    }
    $fields['hashtags'] = mb_encode_numericentity($fields['hashtags'], array(0x0, 0xffff, 0, 0xffff), 'UTF-8');

    $dimensions = imagettfbbox(32, 0, $fontPoliticsHead, $fields['hashtags']);
    $hashtagsWidth = $dimensions[2] - $dimensions[0];
    $hashtagsHeight = $dimensions[1] - $dimensions[7];
    $hashtagsLeft = imagesx($targetImage) - $hashtagsWidth - 10;
    $hashtagsTop = $hashtagsHeight + 17;
    if ($fields['format'] == 'twitter') {
      $hashtagsTop = $hashtagsHeight + 10;
    } elseif ($fields['format'] == 'instagram') {
      $hashtagsTop = $hashtagsHeight + 30;
    }
    imagettftext($textLayer, 32, 0, $hashtagsLeft, $hashtagsTop, $purple, $fontPoliticsHead, $fields['hashtags']);
  }

  // title
  if (!empty($fields['title'])) {
    $titleFontSize = 42;
    $titleSpacing = 14;
    $titleBoxColor = $boxColor;
    $titleLeft = 20;
    $titleTop = -75;
    $titleMaxWidth = imagesx($textLayer) * 0.4;
    $titleMaxLines = 5;
    $titleLineHeight = 1.5;
    $titleCentered = false;
    if ($fields['layout'] == 4) {
      $titleSpacing = 24;
      $titleMaxWidth = imagesx($textLayer) - 40;
      $titleMaxLines = 2;
      $titleCentered = true;
      if ($fields['format'] == 'twitter') {
        $titleLineHeight = 1.6;
        $titleFontSize = 50;
        $titleTop = -imagesy($textLayer) * 0.50;
      } else {
        $titleFontSize = 64;
        $titleTop = -imagesy($textLayer) * 0.55;
      }
    } else {
      if ($fields['format'] == 'twitter') {
        $titleTop = -55;
      } elseif ($fields['format'] == 'instagram') {
        $titleTop = -90;
      }
    }
    if ($fields['image'] == 'empty') {
      $titleBoxColor = null;
    }
    imagettftextmultiline($textLayer, $titleFontSize, 0, $titleLeft, $titleTop, $purple, $fontPoliticsHead, $fields['title'],
      $titleMaxWidth, $titleMaxLines, $titleBoxColor, true, $titleSpacing, $titleLineHeight, $titleCentered);
  }

  // text
  if (!empty($fields['text'])) {
    $textFontSize = 22;
    $textLeft = -8;
    $textTop = 90;
    $textMaxWidth = imagesx($textLayer) * 0.45;
    $textMaxLines = 9;
    $textBoxPerLine = false;
    $textBoxColor = $boxColor;
    $textCentered = false;
    $textLineHeight = 1.3;
    if ($fields['layout'] == 2) {
      $textTop = -140;
      if ($fields['format'] == 'twitter') {
        $textTop = -145;
      } elseif ($fields['format'] == 'instagram') {
        $textTop = -185;
      }
    } elseif ($fields['layout'] == 4) {
      $textLineHeight = 1.5;
      $textLeft = 20;
      $textMaxWidth = imagesx($textLayer) - 40;
      $textMaxLines = 6;
      $textBoxPerLine = true;
      $textCentered = true;
      if ($fields['format'] == 'twitter') {
        $textMaxLines = 4;
        $textFontSize = 20;
        $textTop = imagesy($textLayer) * 0.52;
      } else {
        $textTop = imagesy($textLayer) * 0.45;
      }
    } else {
      if ($fields['format'] == 'twitter') {
        $textTop = 75;
      } elseif ($fields['format'] == 'instagram') {
        $textTop = 120;
      }
    }
    if ($fields['image'] == 'empty') {
      $textBoxColor = null;
    }
    imagettftextmultiline($textLayer, $textFontSize, 0, $textLeft, $textTop, $black, $fontRoboto, $fields['text'],
      $textMaxWidth, $textMaxLines, $textBoxColor, $textBoxPerLine, 8, $textLineHeight, $textCentered);
  }

  // contribution
  if (!empty($fields['contribution'])) {
    $contributionLeft = 15;
    $contributionTop = 80;
    $color = $black;
    (new Logger())->error($fields['contribution-color']);
    if (!empty($fields['contribution-color'])) {
      list($r, $g, $b) = sscanf($fields['contribution-color'], "#%02x%02x%02x");
      $color = imagecolorallocate($textLayer, $r, $g, $b);
    }
    if ($fields['layout'] == 2 || $fields['layout'] == 3) {
      $contributionLeft = -15;
      if ($fields['format'] == 'twitter') {
        $contributionTop = -60;
      } elseif ($fields['format'] == 'instagram') {
        $contributionTop = -100;
      } else {
        $contributionTop = -80;
      }
    } elseif ($fields['layout'] == 4) {
      if ($fields['format'] == 'twitter') {
        $contributionTop = -55;
      } elseif ($fields['format'] == 'instagram') {
        $contributionTop = -100;
      } else {
        $contributionTop = -80;
      }
    } else {
      if ($fields['format'] == 'twitter') {
        $contributionTop = 65;
      } elseif ($fields['format'] == 'instagram') {
        $contributionTop = 110;
      }
    }
    imagettftextmultiline($textLayer, 9, 0, $contributionLeft, $contributionTop, $color, $fontRoboto, $fields['contribution'],
      imagesx($textLayer) * 0.45, 4, null, false, 4, 1.3);
  }

  // insert text layer
  imagecopyresampled($targetImage, $textLayer,
    0, 0, 0, 0,
    imagesx($targetImage), imagesy($targetImage), imagesx($targetImage), imagesy($targetImage));
  imagedestroy($textLayer);

  $targetFile = $tmpDir . 'target_' . uniqid();
  switch ($fields['format']) {
    case "facebook":
    case "twitter":
      $targetFile = $targetFile . ".png";
      imagepng($targetImage, $targetFile);
      break;
    case "instagram":
      $targetFile = $targetFile . ".jpg";
      imagejpeg($targetImage, $targetFile);
      break;
  }
  imagedestroy($targetImage);

  $exampleFileContent = file_get_contents($targetFile);
  $base64 = base64_encode($exampleFileContent);
  unlink($targetFile);
  return $response->withJson(array(
    'success' => true,
    'type' => $fields['format'] == 'instagram' ? 'image/jpg' : 'image/png',
    'data' => $base64
  ));
});

function imagettftextmultiline(&$image, $size, $angle, $left, $top, $color, $font, $text, $max_width, $maxLines, $boxColor, $boxPerLine, $spacing = 20, $linespacing = 1.5, $centered = false)
{
  $wordwidth = array();
  $linewidth = array();
  $linewordcount = array();
  $largest_line_height = 0;
  $largest_line_width = 0;
  $lineno = 0;
  $text = str_replace("\n", " \n", str_replace("\r", "\n", str_replace("\r\n", "\n", $text)));
  $words = explode(" ", $text);
  $wln = 0;
  $linewidth[$lineno] = 0;
  $linewordcount[$lineno] = 0;
  $containsuml = false;
  $isFirstWord = true;
  foreach ($words as $word) {
    $forceNewLine = false;
    if (substr($word, 0, 1) == "\n") {
      $forceNewLine = true;
      $word = substr($word, 1);
    }

    $text_encoding = mb_detect_encoding($word, 'UTF-8, ISO-8859-1');
    if ($text_encoding != 'UTF-8') {
      $word = mb_convert_encoding($word, 'UTF-8', $text_encoding);
    }
    $word = mb_encode_numericentity($word, array(0x0, 0xffff, 0, 0xffff), 'UTF-8');

    $dimensions = imagettfbbox($size, $angle, $font, $word);
    $line_width = $dimensions[2] - $dimensions[0];
    $line_height = $dimensions[1] - $dimensions[7];
    if ($line_height > $largest_line_height) {
      $largest_line_height = $line_height;
    }
    if (($forceNewLine || (($linewidth[$lineno] + $line_width + $spacing) > $max_width && !$isFirstWord)) && ($lineno + 1) < $maxLines) {
      $linewidth[$lineno] = $linewidth[$lineno] - $spacing;
      if ($linewidth[$lineno] > $largest_line_width) {
        $largest_line_width = $linewidth[$lineno];
      }
      $lineno++;
      $linewidth[$lineno] = 0;
      $linewordcount[$lineno] = 0;
      $wln = 0;
    }
    $linewidth[$lineno] += $line_width + $spacing;
    $wordwidth[$lineno][$wln] = $line_width;
    $wordtext[$lineno][$wln] = $word;
    $linewordcount[$lineno]++;
    if (strpos(strtolower($word), 'ä') !== false
      || strpos(strtolower($word), 'ö') !== false
      || strpos(strtolower($word), 'ü') !== false) {
      $containsuml = true;
    }
    $wln++;
    $isFirstWord = false;
  }
  $linewidth[$lineno] = $linewidth[$lineno] - $spacing;
  if ($lineno == 0) {
    $largest_line_width = $linewidth[0];
  }
  $imageHeight = imagesy($image);
  if ($left < 0 || $left == "-0") {
    $left = (imagesx($image) - $largest_line_width) - abs($left);
  }
  $largest_line_height = $containsuml ? $largest_line_height : $largest_line_height + ($size * 0.1);
  if (!empty($boxColor) && !$boxPerLine) {
    $boxOffset = ($top < 0 ? $imageHeight - abs($top) - ($largest_line_height * $linespacing * ($lineno + 1)) : $top);
    imagefilledrectangle($image,
      $left - ($spacing * 3), $boxOffset - ($containsuml ? $spacing : ($spacing / 4)),
      $left + $largest_line_width + ($spacing * 6), ($boxOffset) + ($largest_line_height * $linespacing * ($lineno + 1)) + ($containsuml ? $spacing : ($spacing * 0.8)),
      $boxColor);
  }
  for ($ln = 0; $ln <= $lineno; $ln++) {
    $offset = 0;
    if ($centered) {
      $offset = (imagesx($image) - $linewidth[$ln] - (2 * $spacing)) / 2;
    }
    $x = $offset;
    if ($top < 0) {
      $lntop = $imageHeight - abs($top) - ($largest_line_height * $linespacing * ($lineno - $ln + 1));
    } else {
      $lntop = $top + ($largest_line_height * $ln * $linespacing);
    }
    if (!empty($boxColor) && $boxPerLine) {
      imagefilledrectangle($image,
        $offset + $left - ($spacing / 2),
        $lntop - ($containsuml ? ($spacing / 10) : ($spacing / 4)),
        $offset + $left + $linewidth[$ln] + $spacing,
        $lntop + $largest_line_height + ($containsuml ? ($spacing * 0.6) : ($spacing * 0.8)),
        $boxColor);
    }
    for ($w = 0; $w < $linewordcount[$ln]; $w++) {
      imagettftext($image, $size, $angle, $left + intval($x), $lntop + $largest_line_height, $color, $font, $wordtext[$ln][$w]);
      $x += $wordwidth[$ln][$w] + $spacing;
    }
  }
  return true;
}
