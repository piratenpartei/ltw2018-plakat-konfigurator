<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
require 'Logger.php';
require 'PosterRepository.php';

// init slim:
$app = new \Slim\App(['settings' => include '../../data/_settings.php']);
$container = $app->getContainer();

// error handler:
$container['errorHandler'] = function ($container) {
  return function (Request $request, Response $response, Exception $exception) use ($container) {
    (new Logger())->error('Error: ' . $exception->getMessage());
    return $container->get('response')->withStatus(500)
      ->withHeader('Content-Type', 'application/json')
      ->write(json_encode(array(
        'message' => $container->get('settings')->get('displayErrorDetails') ? $exception->getMessage() : 'Unexpected error!',
        'success' => false
      )));
  };
};

// logger:
$container['log'] = function () {
  return new Logger();
};

// configure database:
$container['db'] = function ($c) {
  $db = $c['settings']['db'];
  $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
    $db['user'], $db['pass']);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
  return $pdo;
};

// enable cors:
include_once '_cors.php';

// routes:
include_once '_routes.php';

// Catch-all route to serve a 404 Not Found page if none of the routes match
// NOTE: make sure this route is defined last
$app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function (Request $request, Response $response) {
  return $response->withJson(array('msg' => 'Not found!', 'success' => false), 404);
});

// run:
$app->run();
