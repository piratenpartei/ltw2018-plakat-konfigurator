<?php

require_once 'Logger.php';

use SVG\SVGImage;
use SVG\Nodes\Shapes\SVGRect;
use SVG\Nodes\Shapes\SVGEllipse;
use SVG\Nodes\Embedded\SVGImageElement;

class PosterRepository
{

  private $db;

  private $settings;

  private $log;

  const EMAIL_FOOTER = "\r\n\r\n\r\nE-Mails an diese Absender-Adresse werden nicht bearbeitet. " .
  "Bitte nutze die Kommentarfunktion auf der Webseite.";

  public function __construct($db, $settings)
  {
    $this->db = $db;
    $this->settings = $settings;
    $this->log = new Logger();
  }

  public function getPoster($code)
  {
    $stmnt = $this->db->prepare(
      "SELECT code, name, description_prefix, description, "
      . "     slogan1, slogan2, slogan3, image_data, notification_email,"
      . "     download_enabled, needs_help, print_data_requested, print_data_requested_a0, print_data_requested_a1, "
      . "     download_link_a0, download_link_a1 "
      . " FROM poster "
      . " WHERE code = ?");
    $stmnt->execute(array($code));
    $poster = $stmnt->fetch();
    if ($poster) {
      $poster['download_enabled'] = $poster['download_enabled'] == 1;
      $poster['needs_help'] = $poster['needs_help'] == 1;
      $poster['print_data_requested'] = $poster['print_data_requested'] == 1;
      $poster['print_data_requested_a0'] = $poster['print_data_requested_a0'] == 1;
      $poster['print_data_requested_a1'] = $poster['print_data_requested_a1'] == 1;
    }
    return $poster;
  }

  public function createPoster($name)
  {
    $idExists = true;
    do {
      $id = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyz"), 0, 7);
      if (!$this->getPoster($id)) {
        $idExists = false;
      }
    } while ($idExists);

    $stmnt = $this->db->prepare("INSERT INTO poster (code, name) VALUES (?, ?)");
    $stmnt->execute(array($id, $name));

    return $this->getPoster($id);
  }

  public function updatePoster($posterCode, $posterData)
  {
    $poster = $this->getPoster($posterCode);

    // send designer emails
    if (!$poster['needs_help'] && $posterData['needs_help']) {
      $emailSubject = 'Hilfe angefordert: ' . $posterCode . ' - Plakatkonfigurator - Piratenpartei';
      $emailBody = "Hallo,\r\n\r\n" .
        "ein Benutzer hat soeben um Hilfe gebeten.\r\n\r\n" .
        "Plakat: " . $this->settings['baseUrl'] . "/poster/p/" . $posterCode . "/5";
      $this->sendDesignerEmail($emailSubject, $emailBody);
    } elseif ($poster['needs_help'] && !$posterData['needs_help']) {
      $emailSubject = 'Keine weitere Hilfe erforderlich: ' . $posterCode . ' - Plakatkonfigurator - Piratenpartei';
      $emailBody = "Hallo,\r\n\r\n" .
        "ein Benutzer hat soeben angegeben, dass er derzeit keine weitere Hilfe mehr benötigt.\r\n\r\n" .
        "Plakat: " . $this->settings['baseUrl'] . "/poster/p/" . $posterCode . "/5";
      $this->sendDesignerEmail($emailSubject, $emailBody);
    }
    if (!$poster['print_data_requested'] && $posterData['print_data_requested']) {
      $emailSubject = 'Druckdaten angefordert: ' . $posterCode . ' - Plakatkonfigurator - Piratenpartei';
      $emailBody = "Hallo,\r\n\r\n" .
        "ein Benutzer hat soeben die Druckdaten für sein Plakat angefordert.\r\n\r\n" .
        "Plakat: " . $this->settings['baseUrl'] . "/poster/p/" . $posterCode . "/5";
      $this->sendDesignerEmail($emailSubject, $emailBody);
    } else if ($poster['print_data_requested'] && !$posterData['print_data_requested']) {
      $emailSubject = 'Druckdaten nicht mehr benötigt: ' . $posterCode . ' - Plakatkonfigurator - Piratenpartei';
      $emailBody = "Hallo,\r\n\r\n" .
        "ein Benutzer hat soeben angegeben, dass er derzeit keine Druckdaten benötigt.\r\n\r\n" .
        "Plakat: " . $this->settings['baseUrl'] . "/poster/p/" . $posterCode . "/5";
      $this->sendDesignerEmail($emailSubject, $emailBody);
    }

    // No validation - technical limits are catched by database - any other mistakes should be allowed here
    $stmnt = $this->db->prepare("UPDATE poster SET "
      . "  name = ? "
      . ", description_prefix = ? "
      . ", description = ? "
      . ", slogan1 = ? "
      . ", slogan2 = ? "
      . ", slogan3 = ? "
      . ", image_data = ? "
      . ", notification_email = ? "
      . ", needs_help = ? "
      . ", print_data_requested = ? "
      . ", print_data_requested_a0 = ? "
      . ", print_data_requested_a1 = ? "
      . " WHERE code = ?");
    $stmnt->execute(array(
      $posterData['name'],
      $posterData['description_prefix'],
      $posterData['description'],
      $posterData['slogan1'],
      $posterData['slogan2'],
      $posterData['slogan3'],
      $posterData['image_data'],
      $posterData['notification_email'],
      $posterData['needs_help'] ? 1 : 0,
      $posterData['print_data_requested'] ? 1 : 0,
      $posterData['print_data_requested_a0'] ? 1 : 0,
      $posterData['print_data_requested_a1'] ? 1 : 0,
      $posterCode
    ));
  }

  public function updatePosterDownloadInfo($posterCode, $downloadInfo)
  {
    // No validation - technical limits are catched by database - any other mistakes should be allowed here
    $stmnt = $this->db->prepare("UPDATE poster SET "
      . "  download_enabled = ? "
      . ", download_link_a0 = ? "
      . ", download_link_a1 = ? "
      . " WHERE code = ?");
    $stmnt->execute(array(
      $downloadInfo['download_enabled'] ? 1 : 0,
      $downloadInfo['download_link_a0'],
      $downloadInfo['download_link_a1'],
      $posterCode
    ));

    $emailSubject = 'Druckdaten wurden aktualisiert - Plakatkonfigurator - Piratenpartei';
    $emailBody = "Hallo,\r\n\r\n" .
      "die Druckdaten für dein Plakat wurden aktualisiert.\r\n\r\n" .
      "Hier findest du den Download-Bereich deines Plakats: " . $this->settings['baseUrl'] . "/poster/p/" . $posterCode . "/6";
    $this->sendUserEmail($posterCode, $emailSubject, $emailBody);
  }

  public function updatePosterImage($code, $imageFile)
  {
    // validate code
    if (!$code || !preg_match('/^[a-z0-9]{7}$/', $code)) {
      return array('response' => array('msg' => 'Missing or invalid poster code!', 'success' => false), 'status' => 400);
    }

    // validate uploaded image
    if (empty($imageFile)) {
      return array('response' => array('msg' => 'Missing file upload!', 'success' => false), 'status' => 400);
    }
    if ($imageFile->getError() !== UPLOAD_ERR_OK) {
      return array('response' => array('msg' => 'Error while uploading file!', 'success' => false), 'status' => 400);
    }
    $uploadFileName = $imageFile->getClientFilename();
    $filenameSuffix = preg_replace("/^.*\.(.+?)$/", "$1", strtolower($uploadFileName));
    if ($filenameSuffix != 'jpg' && $filenameSuffix != 'jpeg' && $filenameSuffix != 'png') {
      return array('response' => array('msg' => 'Invalid image format!', 'success' => false), 'status' => 400);
    }

    // construct target directory and filename
    $cleanFilename = preg_replace("/^(.*)\..+?$/", "$1", strtolower($uploadFileName));
    $cleanFilename = preg_replace("/[^a-z0-9\-_.]+/", "", $cleanFilename);
    $cleanFilename = substr($cleanFilename, 0, min(21, strlen($cleanFilename)));
    $targetDirectory = rtrim($this->settings['filePath'], '/') . '/' . $code . '/';

    $targetFilename = $cleanFilename . '.' . $filenameSuffix;
    $i = 0;
    while (file_exists($targetDirectory . $targetFilename)) {
      $targetFilename = $cleanFilename . '_' . (++$i) . '.' . $filenameSuffix;
    }

    // create directory and write uploaded file
    $imagePath = $targetDirectory . $targetFilename;
    $this->log->debug('Write uploaded file to: ' . $imagePath);
    if (!file_exists($targetDirectory)) {
      mkdir($targetDirectory);
    }
    $imageFile->moveTo($imagePath);

    // save filename to database
    $stmnt = $this->db->prepare("UPDATE poster SET "
      . "  image = ? "
      . " WHERE code = ?");
    $stmnt->execute(array($targetFilename, $code));

    // remove old preview image
    if (file_exists($targetDirectory . 'preview.png')) {
      unlink($targetDirectory . 'preview.png');
    }
    // read original image
    if ($filenameSuffix == 'png') {
      $originalImage = imagecreatefrompng($imagePath);
    } else {
      $originalImage = imagecreatefromjpeg($imagePath);
    }

    // rotate
    $exif = exif_read_data($imagePath);
    $rotatedImage = $originalImage;
    if (!empty($exif['Orientation'])) {
      switch ($exif['Orientation']) {
        case 3:
          $rotatedImage = imagerotate($originalImage, 180, 0);
          imagedestroy($originalImage);
          break;
        case 6:
          $rotatedImage = imagerotate($originalImage, -90, 0);
          imagedestroy($originalImage);
          break;
        case 8:
          $rotatedImage = imagerotate($originalImage, 90, 0);
          imagedestroy($originalImage);
          break;
      }
    }

    // resize image
    $originalHeight = imagesy($rotatedImage);
    $originalWidth = imagesx($rotatedImage);
    $targetHeight = abs(min(600, $originalHeight));
    $targetWidth = abs($targetHeight * ($originalWidth / $originalHeight));
    $resizedImage = imagecreatetruecolor($targetWidth, $targetHeight);
    imagealphablending($resizedImage, FALSE);
    imagesavealpha($resizedImage, TRUE);
    imagecopyresampled($resizedImage, $rotatedImage, 0, 0, 0, 0, $targetWidth, $targetHeight, $originalWidth, $originalHeight);
    imagedestroy($rotatedImage);

    // save image
    imagepng($resizedImage, $targetDirectory . 'preview.png');
    imagedestroy($resizedImage);

    return array('response' => array('success' => true), 'status' => 200);
  }

  public function getPosterImagePreview($code)
  {
    $previewImagePath = rtrim($this->settings['filePath'], '/') . '/' . $code . '/preview.png';
    if (file_exists($previewImagePath)) {
      return base64_encode(file_get_contents($previewImagePath));
    }
    return null;
  }

  public function getPosterMessages($code)
  {
    $stmnt = $this->db->prepare("SELECT sender, message, sendtime "
      . " FROM postermessages "
      . " WHERE code = ? "
      . " ORDER BY sendtime ASC");
    $stmnt->execute(array($code));
    return $stmnt->fetchAll();
  }

  public function savePosterMessages($code, $message, $direction)
  {
    // No validation - technical limits are catched by database - any other mistakes should be allowed here
    $stmnt = $this->db->prepare("INSERT INTO postermessages (code, sender, message) VALUES (?, ?, ?)");
    $stmnt->execute(array(
      $code,
      $direction,
      $message
    ));

    $emailSubject = 'Neue Nachricht - Plakatkonfigurator - Piratenpartei';
    $emailBody = "Hallo,\r\n\r\n" .
      "eine Neue Nachricht wurde gesendet:\r\n\r\n------------------------------------------\r\n" .
      $message .
      "\r\n------------------------------------------\r\n\r\n" .
      "Plakat: " . $this->settings['baseUrl'] . "/poster/p/" . $code . "/5";
    if ($direction == 'out') {
      // send designer email
      $this->sendDesignerEmail($emailSubject, $emailBody);
    } else {
      // send user email
      $this->sendUserEmail($code, $emailSubject, $emailBody);
    }
  }

  public function sharePoster($code, $email)
  {
    $emailSubject = 'Plakat ' . $code . ' - Plakatkonfigurator - Piratenpartei';
    $emailBody = "Hallo,\r\n\r\n" .
      "ein Plakat wurde mit dir geteilt:\r\n\r\n" .
      $this->settings['baseUrl'] . "/poster/p/" . $code .
      "\r\n\r\n\r\nE-Mails an diese Absender-Adresse werden nicht bearbeitet. " .
      "Bitte nutze die Kommentarfunktion auf der Webseite.";
    $emailHeader = "From: " . $this->settings['senderEmail'] . "\r\n" .
      "X-Mailer: PHP/" . phpversion();
    mail($email, $emailSubject, $emailBody, $emailHeader);
  }

  public function getPosterOverview()
  {
    $stmnt = $this->db->prepare("SELECT code, name, needs_help, print_data_requested, print_data_requested_a0, print_data_requested_a1, "
      . " description, slogan1, image, image_data "
      . " FROM poster "
      . " ORDER BY name ASC");
    $stmnt->execute(array());
    return array_map(function ($p) {
      $p['needs_help'] = $p['needs_help'] == 1;
      $p['print_data_requested'] = $p['print_data_requested'] == 1;
      $p['print_data_requested_a0'] = $p['print_data_requested_a0'] == 1;
      $p['print_data_requested_a1'] = $p['print_data_requested_a1'] == 1;

      $p['progress'] = 0;
      if ($p['name'] && strlen($p['name']) >= 5) {
        $p['progress'] += 10;
      }
      if ($p['description'] && strlen($p['description']) >= 5) {
        $p['progress'] += 15;
      }
      if ($p['slogan1'] && strlen($p['slogan1']) >= 5) {
        $p['progress'] += 15;
      }
      if ($p['image'] && strlen($p['image']) >= 1) {
        $p['progress'] += 20;
      }
      if ($p['image_data'] && strlen($p['image_data']) >= 1) {
        $p['progress'] += 20;
      }
      if ($p['print_data_requested'] && !$p['needs_help']) {
        $p['progress'] += 20;
      }
      return $p;
    }, $stmnt->fetchAll());
  }

  public function checkLogin($email, $password)
  {
    $stmnt = $this->db->prepare("SELECT password "
      . " FROM posterdesigner "
      . " WHERE email = ? ");
    $stmnt->execute(array($email));
    return password_verify($password, $stmnt->fetch()['password']);
  }

  public function changePassword($email, $newPassword)
  {
    $stmnt = $this->db->prepare("UPDATE posterdesigner SET "
      . "  password = ? "
      . " WHERE email = ?");
    $stmnt->execute(array(
      password_hash($newPassword, PASSWORD_DEFAULT),
      $email
    ));
  }

  public function createDesigner($email)
  {
    $newPassword = substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!!!?$%-_:.;"), 0, 8);

    $stmnt = $this->db->prepare("INSERT INTO posterdesigner (email, password) VALUES (?, ?)");
    $stmnt->execute(array(
      $email,
      password_hash($newPassword, PASSWORD_DEFAULT)
    ));

    // send email
    $emailSubject = "Neuer Benutzer-Account - Plakatkonfigurator - Piratenpartei";
    $emailBody = "Hallo,\r\n\r\n" .
      "ein neuer Benutzer-Account wurde soeben für dich eingerichtet.\r\n\r\n" .
      "Unter " . $this->settings['baseUrl'] . "/poster/overview kannst du dich nun mit deinen Anmeldedaten einloggen:\r\n\r\n"
      . "    E-Mail: " . $email . "\r\n"
      . "    Passwort: " . $newPassword . "\r\n\r\nDein Passwort solltest du bei der ersten Anmeldung ändern.";

    $emailHeader = "From: " . $this->settings['senderEmail'] . "\r\n" .
      "X-Mailer: PHP/" . phpversion();
    mail($email, $emailSubject,
      $emailBody . self::EMAIL_FOOTER,
      $emailHeader);
  }

  public function sendDesignerEmail($emailSubject, $emailBody)
  {
    $stmnt = $this->db->prepare("SELECT email FROM posterdesigner");
    $stmnt->execute(array());
    $designers = $stmnt->fetchAll();
    $emailHeader = "From: " . $this->settings['senderEmail'] . "\r\n" .
      "X-Mailer: PHP/" . phpversion();
    foreach ($designers as $designer) {
      // send email
      mail($designer['email'], $emailSubject,
        $emailBody . self::EMAIL_FOOTER,
        $emailHeader);
    }
  }

  public function sendUserEmail($posterCode, $emailSubject, $emailBody)
  {
    $poster = $this->getPoster($posterCode);

    if ($poster['notification_email'] && strlen($poster['notification_email'])) {
      $emailHeader = "From: " . $this->settings['senderEmail'] . "\r\n" .
        "X-Mailer: PHP/" . phpversion();
      // send email
      mail($poster['notification_email'], $emailSubject,
        $emailBody . self::EMAIL_FOOTER,
        $emailHeader);
    }
  }

}
