<?php
class Logger {

  private $stdout;
  private $stderr;

  public function __construct() {
    $this->stdout = fopen('php://stdout', 'w');
    $this->stderr = fopen('php://stderr', 'w');
  }

  public function debug($msg) {
    ob_start();
    var_dump($msg);
    fputs($this->stdout, ob_get_clean());
  }

  public function error($msg) {
    ob_start();
    var_dump($msg);
    fputs($this->stderr, ob_get_clean());
  }

}
