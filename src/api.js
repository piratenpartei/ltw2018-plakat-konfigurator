const apiUrl = (path) => '/api' + path;
const CONTENT_TYPE_JSON_UTF8 = 'application/json; charset=UTF-8';

export function createPoster(name) {
  return fetch(apiUrl('/poster'), {
    method: 'post',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify({
      name: name
    })
  }).then(r => r.json()).catch(e => ({success: false, error: e}));
}

export function loadPoster(code) {
  return fetch(apiUrl('/poster/' + encodeURIComponent(code)), {
    method: 'get',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8
    }
  }).then(r => r.json()).catch(e => ({success: false, error: e}));
}

export function updatePoster(posterCode, posterData) {
  return fetch(apiUrl('/poster/' + encodeURIComponent(posterCode)), {
    method: 'put',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify(posterData)
  }).then(r => r.json()).catch(e => ({success: false, error: e}));
}

export function updatePosterDownloadInfo(posterCode, downloadInfo) {
  return fetch(apiUrl('/poster/' + encodeURIComponent(posterCode) + '/download-info'), {
    method: 'put',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify(downloadInfo)
  }).catch(e => ({success: false, error: e}));
}

export function getImagePreview(posterCode) {
  return fetch(apiUrl('/poster/' + encodeURIComponent(posterCode) + '/image/preview'), {
    method: 'get',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8
    }
  }).then(r => r.json()).catch(e => ({success: false, error: e}));
}

export function uploadImage(posterCode, file, progressHandler) {
  // Fetch API does not support progress yet
  // - https://github.com/github/fetch/issues/89
  // - https://github.com/whatwg/fetch/issues/607)
  function futch(url, opts = {}, onProgress) {
    return new Promise((res, rej) => {
      const xhr = new XMLHttpRequest();
      xhr.open(opts.method || 'get', url);
      for (const k in opts.headers || {}) {
        if (opts.headers.hasOwnProperty(k)) {
          xhr.setRequestHeader(k, opts.headers[k]);
        }
      }
      xhr.onload = e => {
        try {
          const result = JSON.parse(e.target.responseText);
          if (result && result.success) {
            res(result);
          } else {
            rej(result);
          }
        } catch (ex) {
          rej(ex);
        }
      };
      xhr.onerror = rej;
      if (xhr.upload && onProgress) {
        xhr.upload.onprogress = onProgress; // event.loaded / event.total * 100 ; //event.lengthComputable
      }
      xhr.send(opts.body);
    });
  }

  const data = new FormData();
  data.append('foo', 'bar');
  data.append('file', file);

  return futch(apiUrl('/poster/' + encodeURIComponent(posterCode) + '/image'), {
    method: 'POST',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8
    },
    body: data
  }, (event) => progressHandler(event.loaded / event.total * 100)).catch(
    e => ({success: false, error: e})
  );
}

export function loadMessages(code) {
  return fetch(apiUrl('/poster/' + encodeURIComponent(code) + '/messages'), {
    method: 'get',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8
    }
  }).then(r => r.json()).catch(e => ({success: false, error: e}));
}

export function sendMessage(code, message) {
  return fetch(apiUrl('/poster/' + encodeURIComponent(code) + '/messages'), {
    method: 'post',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify({
      message: message
    })
  }).then(r => r.json()).catch(e => ({success: false, error: e}));
}

export function sharePoster(code, email) {
  return fetch(apiUrl('/poster/' + encodeURIComponent(code) + '/share'), {
    method: 'post',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify({
      email: email
    })
  }).catch(e => ({success: false, error: e}));
}

export function posterLogin(login) {
  return fetch(apiUrl('/poster/login'), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify(login)
  }).catch(e => ({success: false, error: e}));
}

export function getPosterUserInfo() {
  return fetch(apiUrl('/poster/login/info'), {
    method: 'get',
    credentials: 'same-origin',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8
    }
  }).catch(e => ({success: false, error: e}));
}

export function posterLogout() {
  return fetch(apiUrl('/poster/logout'), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8
    }
  }).catch(e => ({success: false, error: e}));
}

export function changePosterPassword(passwords) {
  return fetch(apiUrl('/poster/login/password'), {
    method: 'put',
    credentials: 'same-origin',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify(passwords)
  }).catch(e => ({success: false, error: e}));
}

export function posterOverview() {
  return fetch(apiUrl('/poster/overview'), {
    method: 'get',
    credentials: 'same-origin',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8
    }
  }).catch(e => ({success: false, error: e}));
}

export function createUser(email) {
  return fetch(apiUrl('/poster/login/create'), {
    method: 'post',
    credentials: 'same-origin',
    headers: {
      'Accept': CONTENT_TYPE_JSON_UTF8,
      'Content-Type': CONTENT_TYPE_JSON_UTF8
    },
    body: JSON.stringify({email})
  }).catch(e => ({success: false, error: e}));
}
